elements = [].slice.call document.querySelectorAll "*[data-delayed-src]"
cached = elements
  .map (element) ->
    offsetTop = ig.utils.offset element .top
    visible = no
    {element, offsetTop, visible}
  .sort (a, b) -> a.offsetTop - b.offsetTop
currentLength = cached.length
margin = 500

height = window.innerHeight
reCache = ->
  for obj in cached
    obj.offsetTop = ig.utils.offset element .top
  height := window.innerHeight


bounceTimeout = null
debounce = (timeout, fn) ->
  return if bounceTimeout
  bounceTimeout := setTimeout do
    ->
      bounceTimeout := null
      fn!
    timeout

onscroll = ->
  return unless currentLength
  top = (document.body.scrollTop || document.documentElement.scrollTop)
  i = 0
  while cached[i]
    if cached[i].offsetTop > top + height + margin
      break
    else
      i++
  countToDisplay = i
  if countToDisplay > 0
    itemsToDisplay = cached.slice 0, countToDisplay
    for {element} in itemsToDisplay
      element.setAttribute \src element.getAttribute \data-delayed-src
    cached .= slice countToDisplay
    currentLength := cached.length


window.addEventListener \scroll ->
  debounce 500 onscroll

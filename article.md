---
title: "Existují Sudety? Hranice jsou zřetelné i sedmdesát let po vysídlení Němců"
perex: "Statistiky nezaměstnanosti, kvality vzdělání nebo volební účasti v&nbsp;Česku dodnes kopírují hranice historických Sudet. Vlnu osadníků připomíná také nejsilnější příhraniční menšina – Slováci. Prohlédněte si detailní mapy."
authors: ["Jan Boček", "Jan Cibulka"]
published: "10. června 2016"
coverimg: https://samizdat.cz/data/sudety-clanek/www/media/vysidleni.jpg
url: "sudety"
description: "Přesně před sedmdesáti lety začal vládou schválený odsun sudetských Němců do sovětského pásma. Tři miliony německých starousedlíků rychle nahradili noví osadníci: Češi z vnitrozemí, Slováci, Maďaři, navrátivší se čeští emigranti, ale také Řekové a Makedonci. Statistiky nezaměstnanosti, kvality vzdělání nebo volební účasti v Česku dodnes kopírují hranice historických Sudet. Prohlédněte si podrobné mapy."
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
---

Přesně před sedmdesáti lety začal vládou schválený odsun sudetských Němců do sovětského pásma. Tři miliony německých starousedlíků rychle nahradili noví osadníci: Češi z vnitrozemí, Slováci, Maďaři, navrátivší se čeští emigranti, ale také Řekové a Makedonci.

Pohraničí je dodnes jiné než zbytek země. Podobně, jako je v německých statistikách stále zřetelné [rozdělení na západní a východní část země](https://www.washingtonpost.com/news/worldviews/wp/2014/10/31/the-berlin-wall-fell-25-years-ago-but-germany-is-still-divided/) a [očekávaná délka života stále respektuje železnou oponu](http://data.blog.ihned.cz/c1-62250230-data-na-vychode-byvale-zelezne-opony-se-umira-o-dva-az-sest-let-driv), vystupují z mapy Česka bývalé Sudety.

Každá z oblastí bývalých Sudet přitom má svoje specifické problémy. Severní Čechy, někdy kvůli svému předválečnému průmyslu označované „bohaté Sudety“, mají dnes jiné trápení než rozsáhlé jesenické pohraničí nebo Šumava. Podívejte se, s čím se která část pohraničí potýká.


<aside class="small">
  <h3>Vysídlení sudetských Němců ve vysílání Československého a Českého rozhlasu</h3>
  <ul>
    <li>18. července 1946: <a href="https://samizdat.blob.core.windows.net/zvuky/1946-benes.mp3" target="_blank">Projev prezidenta Dr. E. Beneše v Hrabyni</a></li>
    <li>29. října 1946: <a href="https://samizdat.blob.core.windows.net/zvuky/1946-nosek-karlovy-vary.mp3" target="_blank">Ministr vnitra Václav Nosek hovoří k odjezdu posledního transportu sudetských Němců z Karlových Varů</a></li>
    <li>5. listopadu 1946: <a href="https://samizdat.blob.core.windows.net/zvuky/1946-nosek-projev.mp3" target="_blank">Projev ministra vnitra Václava Noska k odsunu Němců</a></li>
    <li>30. prosince 1969: <a href="https://samizdat.blob.core.windows.net/zvuky/1969-nemecka-otazka.mp3" target="_blank">Německá otázka v Československu (Aktuální úkoly národnostní politiky KSČ)</a></li>
    <li>12. července 1979: <a href="https://samizdat.blob.core.windows.net/zvuky/1979-kamapan.mp3" target="_blank">O kampani rozpoutané v NSR – odsun Němců z pohraničí</a></li>
    <li>29. března 1993: <a href="https://samizdat.blob.core.windows.net/zvuky/1993-havel-ozveny.mp3" target="_blank">Václav Havel – Schvalovat odsun sudetských Němců je čin morálně vadný (Ozvěny dne)</a></li>
    <li>10. října 1993: <a href="https://samizdat.blob.core.windows.net/zvuky/1993-havel.mp3" target="_blank">Václav Havel o sudetsko-německé otázce (Hovory v Lánech)</a></li>
    <li>1995: <a href="https://samizdat.blob.core.windows.net/zvuky/1995-klaus.mp3" target="_blank">Václav Klaus k odsunu sudetských Němců</a></li>
    <li>31. května 2005: <a href="https://samizdat.blob.core.windows.net/zvuky/2005-postoloprty.mp3" target="_blank">Zapomenutý květen v Postoloprtech - reportážní koláž o projektu perzekuce.cz</a></li>
    <li>19. listopadu 2010: <a href="https://samizdat.blob.core.windows.net/zvuky/2010-ozveny.mp3" target="_blank">V médiích sílí debaty o odsunu Němců po válce (Ozvěny dne)</a></li>
    <li>Červen 2016: <a href="https://samizdat.blob.core.windows.net/zvuky/2016-doopravdy.mp3" target="_blank">Jak to bylo doopravdy – historik Oldřich Tůma</a></li>
  </ul>
</aside>

## Slovenská stopa je v pohraničí nejsilnější

Koncem března 1947 žilo v Sudetech podle dobových dokumentů 2,5 milionu lidí, přibližně o půl milionu méně než před válkou. Třetina z nich byli původní obyvatelé, třetina Čechoslováci z vnitrozemí a třetina národnostní a etnické menšiny.

Nejčastějším jazykem po češtině se brzy stala slovenština. Hned v první vlně přistěhovalců sem zamířilo přibližně 200 tisíc slovenských osadníků včetně několika desítek tisíc východoslovenských Romů. Další následovali – příděly půdy zdarma zlákaly české a slovenské emigranty z Polska, Vídně, Maďarska, rumunských osad nebo Podkarpatské Rusi. Část z nich měla o svém cíli jen matnou představu.

„To, že nejedou na Slovensko, odkud jejich rody pocházely, se rumunští Slováci, kteří tvořili největší část této vlny ‚reemigrantů‘, dozvěděli nejdříve ve vlaku,“ popisuje jejich trampoty [Matěj Spurný v knize Sudetské příběhy](http://www.antikomplex.cz/spolecnost-v-sudetech-po-roce-1945.html). „Desetitisíce těchto lidí – jen Slováků žilo v Rumunsku okolo 50 tisíc, přičemž není jasné, kolik přesně jich přesídlilo do Sudet – byly rozvezeny do Jeseníků, do zapadlých koutů jižních Čech a jižní Moravy a do pustých oblastí západočeského pohraničí. Dostali většinou tři hektary půdy, přičemž se počítalo s tím, že se z nich stanou lesní dělníci.“

Celkem do bývalých Sudet zamířilo kolem 300 tisíc etnických Slováků. A právě jejich stopa je dodnes v příhraničí nejvýraznější. Ke slovenské národnosti se při sčítání obyvatel v roce 2011 přihlásilo téměř 40 tisíc zdejších obyvatel.

Hranice Sudet v mapě kopíruje hranici, kde ve třicátých letech mluvilo víc obyvatel německy než česky (vycházíme z [jazykové mapy](http://nemci.euweb.cz/m2.gif), původně vydané v publikaci Ortslexikon Sudetenland v roce 1987). Pohled na mapu prozrazuje, že čeští Slováci dnes žijí v podobných hranicích jako dříve čeští Němci. Je jich ovšem řádově méně; Němci tvořili v Sudetech většinu, ke slovenské národnosti se zde hlásí necelá dvě procenta lidí.

<aside class="big">
  <figure>
    <img data-delayed-src="https://samizdat.cz/data/sudety-clanek/www/media/slovaci.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Zdroj: <a href="https://vdb.czso.cz/vdbvo2/faces/cs/index.jsf?page=statistiky#katalog=30261" target="_blank">Sčítání lidu, domů a bytů 2011</a>
  </figcaption>
</aside>

Navzdory důkladnému odsunu v pohraničí zůstala také část sudetských Němců. Poválečné Československo je potřebovalo, ovládali taje sklářského nebo textilního průmyslu; ti se naopak dlouho odstěhovat nesměli. „Koncem šedesátých let mohli požádat o vystěhování a po zaplacení několika desítek tisíc korun také odejít,“ [vypráví o německé komunitě v krušnohorských Abertamech](http://www.respekt.cz/tydenik/2013/14/posledni-nemci-v-sudetech) novinář Tomáš Lindner v časopise Respekt. Část zůstala, jejich potomci dnes tvoří v českém pohraničí druhou největší menšinu. K německé národnosti se přihlásilo přes 13 tisíc místních obyvatel.

<aside class="small">
  <figure>
    <iframe src="https://samizdat.cz/dw/wuQRG/" class="ig" width="300" height="325" scrolling="no" frameborder="0"></iframe>
  </figure>
</aside>

Ostatní národy, které se v českém příhraničí po válce ocitly, výraznou stopu nezanechaly. Desítky tisíc Maďarů během sedmdesáti let buď odešly, nebo se přestaly hlásit ke svému původu. Podobný osud zřejmě potkal také 20 tisíc řeckých komunistů, kteří sem zamířili po prohrané občanské válce ve své zemi. Ti na začátku padesátých let osídlili hlavně sever Moravy; do dvacetitisícového Bruntálu jich přišly dva a půl tisíce. Při posledním sčítání obyvatel se ovšem k řecké národnosti mnoho z jejich potomků nepřihlásilo.

## Severní Morava zůstala pustá

Před válkou patřilo pohraničí navzdory náročným přírodním podmínkám k nejhustěji obydleným oblastem Československa. Po roce 1945 tu zůstalo pouze půl milionu Čechoslováků a několik desítek tisíc Němců, celkem asi pětina původních obyvatel.

Při poválečném osídlování Sudet neměly všechny oblasti stejnou prioritu: klíčové pro tehdejší autority bylo osídlení průmyslových severních Čech. Severo a jihomoravské pohraničí mělo nižší prioritu, přestože i zde mělo dojít k nahrazení původního obyvatelstva v plné síle. Místo průmyslu tady ovšem pováleční plánovači kladli důraz na rekreační využití.

Šumava, Český les a jihočeské pohraničí měly naopak zůstat vysídlené. Pro komunisty byla hranice se západním Německem pravděpodobným budoucím bojištěm, místem, kde by v případě globálního konfliktu došlo k prvním vážným střetům.

Dnešní statistika hustoty osídlení ukazuje, že ne všechno šlo podle plánu.

<aside class="big">
  <figure>
    <img data-delayed-src="https://samizdat.cz/data/sudety-clanek/www/media/hustota.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Zdroj: <a href="https://vdb.czso.cz/vdbvo2/faces/cs/index.jsf?page=statistiky#katalog=30261" target="_blank">Sčítání lidu, domů a bytů 2011</a>
  </figcaption>
</aside>

Severní Čechy, kam zamířila první a největší vlna osadníků, se dosídlit podařilo, byť nerovnoměrně. To nemuselo být špatně – komunističtí plánovači žádali obsazení průmyslových oblastí, zemědělství v severočeských kopcích nepovažovali za perspektivní a vesnice tedy mohly zůstat pusté. Co se však nepodařilo vůbec, bylo osídlení severní Moravy. Ta zůstává spolu se Šumavou a Českým lesem nejřidčeji osídleným regionem v zemi.

Přes nízkou hustotu osídlení je dnes hlavně na západě a na jihu tehdejších Sudet o bydlení nouze. K paradoxní situaci, kdy původně prázdné vesnice nahradil dnešní nedostatek bytů, přispěl také rok 1959. Tehdy komunistická vláda poručila strhnout v této oblasti všechna opuštěná stavení; čistce padlo za oběť přes 34 tisíc objektů. Některé z nich najdete na [mapě vylidněných obcí a zničených památek](https://www.google.com/maps/d/viewer?mid=zXKjYP6afxt0.k2sUmw2-2N4w&hl=en_US). Databáze webu zanikleobce.cz, ze které mapa čerpá, uvádí víc než sedm stovek zpustlých nebo zničených vesnic.

## O čtvrtinu víc lidí bez práce

Také ekonomicky patřily do konce třicátých let Sudety k nejvyspělejším částem země. „Vyhnání 2,6 milionu českých Němců znamenalo ztrátu jednoho milionu osob v produktivním věku: šlo o pracovité zemědělce i drobné řemeslníky, tovární dělníky i o vzdělanou inteligenci, jež byla součástí české kultury. Nacházela se zde třetina orné půdy a více než tři čtvrtiny luk a pastvin, většina vinic, více než třetina lesů, velká část vodních ploch a rybníků,“ vyjmenovává Matěj Spurný.

Za komunismu, který se chlubil stoprocentní zaměstnaností, ještě veřejné statistiky hospodářské selhání pohraničí zatajovaly. Po roce 1989 se jeho zaostalost projevila naplno, například nezaměstnanost zde rostla rychleji než v jiných částech země. Poslední sčítání obyvatel ukazuje, že v roce 2011 byla v bývalých Sudetech nezaměstnanost o čtvrtinu vyšší než ve vnitrozemí (12,4 % vs. 9,2 %).

<aside class="big">
  <figure>
    <img data-delayed-src="https://samizdat.cz/data/sudety-clanek/www/media/nezamestnanost.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Zdroj: <a href="https://www.czso.cz/" target="_blank">Český statistický úřad</a>
  </figcaption>
</aside>

Zvlášť patrný je nedostatek práce v severomoravské části Sudet, zbytek pohraničí na tom ale není o mnoho lépe. Ve stejných regionech najdeme také nejvíc rodin pod hladinou chudoby, v severní části země navíc stát vyplácí nadprůměrné sociální dávky.

Nepotvrdila se ovšem představa, že pohraničí plošně trpí vyšší majetkovou nebo násilnou kriminalitou. Ta se koncentruje do velkých měst (nejvyšší je v Praze) na celém území Česka. Řidčeji osídlené Šumavě, Českému lesu a Jeseníkům se tedy kriminalita spíše vyhýbá, vyšší čísla ukazují pouze severní Čechy.

## Základní škola stačí

Vyšší nezaměstnanost souvisí s další charakteristikou oblasti: nízkou vzdělaností. Je tu z celého Česka největší podíl lidí, kteří dokončili pouze základní školu.

<aside class="big">
  <figure>
    <img data-delayed-src="https://samizdat.cz/data/sudety-clanek/www/media/vzdelani.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Zdroj: <a href="https://vdb.czso.cz/vdbvo2/faces/cs/index.jsf?page=statistiky#katalog=30261" target="_blank">Sčítání lidu, domů a bytů 2011</a>
  </figcaption>
</aside>

## Volí méně lidí a hlasují proti systému

Volební účast v parlamentních volbách je v pohraničí o pět až deset procentních bodů nižší, než je obvyklé ve vnitrozemském Česku. Jsou zde také úspěšnější protestní strany, včetně komunistů.

<aside class="big">
  <figure>
    <img data-delayed-src="https://samizdat.cz/data/sudety-clanek/www/media/ucast.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Zdroj: <a href="http://volby.cz/pls/ps2013/ps" target="_blank">Volby.cz</a>
  </figcaption>
</aside>

Popularita komunistů tady zakořenila hned po osídlení. „Velká masa obyvatelstva bez tradic a ustálených majetkových i jiných vztahů byla ideálním potenciálním voličstvem KSČ, která s těmito lidmi sdílela étos nového začátku a revolučních změn,“ píše Matěj Spurný ze spolku Antikomplex. „To ostatně plně potvrdily volby na jaře roku 1946, kdy v pohraničí strana docílila volebního zisku 53 procent oproti průměrným 40 procentům v celých českých zemích.“

V mnoha západočeských okresech se komunistům daří oslovovat voliče i dnes. Jedním z důvodů je i fakt, že blízkost hranic se Západním Německem vyžadovala nastěhovat sem „nejspolehlivější“ rodiny a pohraničníky. Jak ale ukazuje [podrobná mapa evropských voleb v roce 2014](http://bl.ocks.org/michalskop/raw/df2c64ef89e9a5f9f41d/), každá část pohraničí volila jinak: v severních Čechách vítězilo převážně ANO, v západní Čechách a na Šumavě i díky výletníkům TOP 09 a na Jesenicku jednoznačně ČSSD. V okolí Znojma se rozhodovalo mezi ANO, komunisty a křesťanskými demokraty.

Bývalé území Sudet tedy těžko můžeme považovat za monolit. Přestože je trápí podobné problémy, každá část bývalých Sudet se chová – nejen ve volbách – mírně odlišně.
